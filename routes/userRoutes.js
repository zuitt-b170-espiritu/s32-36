const express=require("express")
const router=express.Router()


const auth=require("../auth.js")
const userController=require("../controllers/userController.js")


/*
Mini activity
create a function with the "/checkEmail" endpoint that will be able to use the checkEmail function in the userController
Reminder that we have a parameter needed in the checkEmail function and that is data from request body
*/


router.post("/checkEmail",(req,res)=>{
	userController.checkEmail(req.body).then(resultFromController=>res.send(resultFromController))
})

// user registration
router.post("/register", ( req, res ) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// user login
router.post("/login", ( req, res ) => {
	userController.userLogin(req.body).then(resultFromController => res.send(resultFromController))
})

// auth.verify - ensures that a user is logged in before proceeding to the next part of the code; this is the verify function inside the auth.js
router.get("/details", auth.verify, (req, res)=> {
	// decode - decrypts the token inside the authorization (which is in the headers of the request) 
	// req.headers.authorization contains the token that was created for the user
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	userController.getProfile( {userId: userData.id} ).then(resultFromController => res.send(resultFromController))
} )


module.exports=router