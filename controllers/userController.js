// set up dependencies
const User=require("../models/user.js")
const Course=require("../models/course.js")
const auth=require("../auth.js")
const bcrypt=require("bcrypt") //used to encrypt user password


// check if the email exists
/*
	1.check for the email in the database
	2.send the result as a response(with error handling)
*/
/*
	it is conventional for the devs to use Boolean in sending return responses esp with the backend application
*/

module.exports.checkEmail=(requestBody)=>{
	return User.find({email:requestBody.email}).then(result=>{
		if(error){
			console.log(error)
			return false
		}else{
			if(result.length>0){
				// return result
				return true
			}else{
				// return res.send(email does not exist)
				return false
			}
		}
	})
}


/*
USER REGISTRATION
1. create a new User object with the information from the requestBody
2. make sure that the password is encrypted
3. save the new user
*/
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		age: reqBody.age,
		gender: reqBody.gender,
		email: reqBody.email,
		// hashSync is a function of bcrypt that encrypts the password
			// 10 the number of rounds/times it runs the algorithm to the reqBody.password
				// max 72 implementations
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})
	return newUser.save().then((saved, error) =>{
		if (error){
			console.log(error)
			return false
		}else{
			return true
		}
	})
}

// USER LOGIN
/*
1.  find if the email is existing in the database
2. check if the password is correct
*/

module.exports.userLogin = (reqBody) => {
	return User.findOne( { email: reqBody.email } ).then(result => {
		if (result === null) {
			return false
		} else {
			// compareSync function - used to compare a non-encrypted password to an encrypted password and retuns a Boolean reasponse depending on the result
			/*
			What should we do after the comparison?
				true - a token should be created since the user is existing and the password is correct
				false - the passwords do not match, thus a toke should not be created
			*/
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result.toObject())}
				// auth - imported auth.js
				// createAccessToken - function inside the auth.js to create access token
				// .toObject - transforms the User into an Object that can be used by our createAccessToken
			} else {
				return false
			}
		}
	})
}

/*
miniactivity
	create getProfile function inside the userController
		1. find the id of the user using "data" as parameters
		2. if it does not exist, return false,
		3. otherwise, return the details (stretch - make the password invisible)
*/
module.exports.getProfile = (data) => {
	// find the user by the id
	return User.findById( data.userId ).then(result =>{
		// if there is no user found...
		if (result === null) {
			return false
		// if a user is found...
		} else {
			result.password = "";
			return result
		}
	})
}
